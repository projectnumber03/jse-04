import org.junit.Test;

import java.util.*;

public class ApplicationTest {
    @Test
    public void projectCreateTest() {
        /*for (final ClassPath.ClassInfo info : ClassPath.from(Thread.currentThread().getContextClassLoader()).getTopLevelClasses()) {
            if (info.getName().startsWith("ru.shilov.tm.entity")) {
                final Class<?> clazz = info.load();
                if (!clazz.getSuperclass().equals(Object.class)) {
                    Entity entity = (Entity)clazz.newInstance().getClass().newInstance();
                    System.out.println(clazz.getName());
                }
            }
        }*/

        /*Reflections reflections = new Reflections("ru.shilov.tm.entity");

        Set<Class<? extends Entity>> subTypes = reflections.getSubTypesOf(Entity.class);

        subTypes.forEach(s -> {
            try {
                Entity entity = s.newInstance();
                System.out.println(entity.getClass());
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        });*/
    }

    private String getTestData() {
        StringJoiner commands = new StringJoiner("\n");
        commands.add("project-1");
        commands.add("Description of project-1");
        commands.add("02.11.2015");
        commands.add("03.11.2015");
        commands.add("project-2");
        commands.add("Description of project-2");
        commands.add("04.11.2015");
        commands.add("05.11.2015");
        commands.add("all");
        commands.add("task-1");
        commands.add("Description of task-1");
        commands.add("02.11.2015");
        commands.add("03.11.2015");
        commands.add("task-2");
        commands.add("Description of task-2");
        commands.add("02.11.2015");
        commands.add("03.11.2015");
        commands.add("task-3");
        commands.add("Description of task-3");
        commands.add("02.11.2015");
        commands.add("03.11.2015");
        return commands.toString();
    }
}
