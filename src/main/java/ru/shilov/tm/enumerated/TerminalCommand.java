package ru.shilov.tm.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum TerminalCommand {

    PROJECT_CLEAR("Удаление всех проектов"),
    PROJECT_CREATE("Создание проекта"),
    PROJECT_LIST("Список проектов"),
    PROJECT_REMOVE("Удаление проекта"),
    PROJECT_EDIT("Переименование проекта"),
    PROJECT_SELECT("Свойства проекта"),
    TASK_CLEAR("Удаление всех задач"),
    TASK_CREATE("Создание задачи"),
    TASK_LIST("Список задач"),
    TASK_REMOVE("Удаление задачи"),
    TASK_EDIT("Переименование задачи"),
    TASK_SELECT("Свойства задачи"),
    TASK_ATTACH("Добавление задачи в проект"),
    HELP("Список доступных комманд"),
    EXIT("Завершение работы");

    @Getter
    private String description;

}
