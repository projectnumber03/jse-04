package ru.shilov.tm.repository;

import java.util.List;
import java.util.Optional;

public interface IRepository<T> {

    List<T> findAll();

    Optional<T> findOne(String id);

    void removeAll();

    T remove(String id);

    T persist(T obj);

    T merge(T obj);

    String getId(String value);

}
