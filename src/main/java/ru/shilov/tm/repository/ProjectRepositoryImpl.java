package ru.shilov.tm.repository;

import lombok.Getter;
import ru.shilov.tm.entity.Project;
import java.util.*;

public class ProjectRepositoryImpl implements IRepository<Project> {

    @Getter
    private static final IRepository<Project> instance = new ProjectRepositoryImpl();

    private Map<String, Project> projects = new LinkedHashMap<>();

    private ProjectRepositoryImpl() {
    }

    @Override
    public List<Project> findAll() {
        return new ArrayList<>(projects.values());
    }

    @Override
    public Optional<Project> findOne(String id) {
        return findAll().stream().filter(p -> p.getId().equals(id)).findAny();
    }

    @Override
    public void removeAll() {
        projects.clear();
    }

    @Override
    public Project remove(String id) {
        return projects.remove(id);
    }

    @Override
    public Project persist(Project project) {
        project.setId(UUID.randomUUID().toString());
        return projects.put(project.getId(), project);
    }

    @Override
    public Project merge(Project project) {
        return projects.put(project.getId(), new Project(project));
    }

    @Override
    public String getId(String value) {
        return new ArrayList<>(projects.keySet()).get(Integer.parseInt(value) - 1);
    }

}
