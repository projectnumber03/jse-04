package ru.shilov.tm.repository;

import lombok.Getter;
import ru.shilov.tm.entity.Task;
import java.util.*;

public class TaskRepositoryImpl implements IRepository<Task> {

    @Getter
    private static final IRepository<Task> instance = new TaskRepositoryImpl();

    private Map<String, Task> tasks = new LinkedHashMap<>();

    private TaskRepositoryImpl() {
    }

    @Override
    public List<Task> findAll() {
        return new ArrayList<>(tasks.values());
    }

    @Override
    public Optional<Task> findOne(String id) {
        return findAll().stream().filter(t -> t.getId().equals(id)).findAny();
    }

    @Override
    public void removeAll() {
        tasks.clear();
    }

    @Override
    public Task remove(String id) {
        return tasks.remove(id);
    }

    @Override
    public Task persist(Task task) {
        task.setId(UUID.randomUUID().toString());
        return tasks.put(task.getId(), task);
    }

    @Override
    public Task merge(Task task) {
        return tasks.put(task.getId(), new Task(task));
    }

    @Override
    public String getId(String value) {
        return new ArrayList<>(tasks.keySet()).get(Integer.parseInt(value) - 1);
    }

}
