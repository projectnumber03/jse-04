package ru.shilov.tm.service;

import ru.shilov.tm.entity.Task;
import java.util.List;
import java.util.Optional;

public class TaskServiceImpl implements IService<Task> {

    @Override
    public List<Task> findAll() {
        return taskRepo.findAll();
    }

    @Override
    public Optional<Task> findOne(String id) {
        return taskRepo.findOne(id);
    }

    @Override
    public void removeAll() {
        taskRepo.removeAll();
    }

    @Override
    public Task remove(String id) {
        if (findOne(id).isPresent()) {
            return taskRepo.remove(id);
        }
        throw new RuntimeException("remove error");
    }

    @Override
    public Task persist(Task task) {
        if (!findOne(task.getId()).isPresent()) {
            return taskRepo.persist(task);
        }
        throw new RuntimeException("persist error");
    }

    @Override
    public Task merge(Task task) {
        if (findOne(task.getId()).isPresent()) {
            return taskRepo.merge(task);
        }
        throw new RuntimeException("merge error");
    }

    @Override
    public String getId(String value) {
        if (checkValue(value)) {
            return taskRepo.getId(value);
        }
        throw new RuntimeException("getting id error");
    }

}
