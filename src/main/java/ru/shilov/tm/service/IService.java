package ru.shilov.tm.service;

import com.google.common.base.Strings;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.repository.ProjectRepositoryImpl;
import ru.shilov.tm.repository.IRepository;
import ru.shilov.tm.repository.TaskRepositoryImpl;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface IService<T> extends IRepository<T> {

    IRepository<Project> projectRepo = ProjectRepositoryImpl.getInstance();

    IRepository<Task> taskRepo = TaskRepositoryImpl.getInstance();

    default String getAllProjectsWithTasks() {
        return IntStream.range(1, projectRepo.findAll().size() + 1).boxed()
                .map(i -> (i + ". " + getProjectWithTasks(projectRepo.findAll().get(i - 1).getId())))
                .collect(Collectors.joining("\n"));
    }

    default String getProjectWithTasks(String id) {
        List<Task> tasks = taskRepo.findAll().stream()
                .filter(t -> t.getIdProject().equals(id))
                .collect(Collectors.toList());
        return projectRepo.findOne(id).get().getName().concat("\n")
                .concat(IntStream.range(1, tasks.size() + 1).boxed()
                .map(i -> String.format("\t%d. %s", i, tasks.get(i - 1).getName()))
                .collect(Collectors.joining("\n")));
    }

    default String getAllTasks() {
        return IntStream.range(1, taskRepo.findAll().size() + 1).boxed()
                .map(i -> (String.format("%d. %s", i, taskRepo.findAll().get(i - 1).getName())))
                .collect(Collectors.joining("\n"));
    }

    default boolean checkValue(String value) {
        return value.matches("\\d+") && findAll().size() >= Integer.parseInt(value);
    }

}
