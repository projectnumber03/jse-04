package ru.shilov.tm.service;

import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProjectServiceImpl implements IService<Project> {

    @Override
    public List<Project> findAll() {
        return projectRepo.findAll();
    }

    @Override
    public Optional<Project> findOne(String id) {
        return projectRepo.findOne(id);
    }

    @Override
    public void removeAll() {
        projectRepo.removeAll();
    }

    @Override
    public Project remove(String id) {
        if (findOne(id).isPresent()) {
            projectRepo.remove(id);
            taskRepo.findAll().stream()
                    .filter(t -> t.getIdProject().equals(id))
                    .map(Task::getId)
                    .collect(Collectors.toList())
                    .forEach(taskRepo::remove);
        }
        throw new RuntimeException("remove error");
    }

    @Override
    public Project persist(Project project) {
        if (!findOne(project.getId()).isPresent()) {
            return projectRepo.persist(project);
        }
        throw new RuntimeException("persist error");
    }

    @Override
    public Project merge(Project project) {
        if (findOne(project.getId()).isPresent()) {
            return projectRepo.merge(project);
        }
        throw new RuntimeException("merge error");
    }

    @Override
    public String getId(String value) {
        if (checkValue(value)) {
            return projectRepo.getId(value);
        }
        throw new RuntimeException("getting id error");
    }

}
