package ru.shilov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

import static ru.shilov.tm.Bootstrap.DATE_PATTERN;

@Getter
@Setter
@NoArgsConstructor
public class Task extends Entity {

    private String name;

    private String description;

    private LocalDate start;

    private LocalDate finish;

    private String idProject = "";

    public Task(Task task) {
        super(task);
        this.name = task.name;
        this.description = task.description;
        this.start = task.start;
        this.finish = task.finish;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("задача: ").append(this.name).append("\n");
        sb.append("описание: ").append(this.description).append("\n");
        sb.append("дата начала: ").append(DATE_PATTERN.format(this.start)).append("\n");
        sb.append("дата окончания: ").append(DATE_PATTERN.format(this.finish));
        return sb.toString();
    }

}
