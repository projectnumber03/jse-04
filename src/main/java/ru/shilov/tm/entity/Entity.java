package ru.shilov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
abstract class Entity {

    private String id;

    protected Entity(Entity entity) {
        this.id = entity.id;
    }

}
