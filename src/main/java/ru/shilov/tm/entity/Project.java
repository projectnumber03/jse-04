package ru.shilov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

import static ru.shilov.tm.Bootstrap.DATE_PATTERN;

@Getter
@Setter
@NoArgsConstructor
public class Project extends Entity {

    private String name;

    private String description;

    private LocalDate start;

    private LocalDate finish;

    public Project(Project project) {
        super(project);
        this.name = project.name;
        this.description = project.description;
        this.start = project.start;
        this.finish = project.finish;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("проект: ").append(this.name).append("\n");
        sb.append("описание: ").append(this.description).append("\n");
        sb.append("дата начала: ").append(DATE_PATTERN.format(this.start)).append("\n");
        sb.append("дата окончания: ").append(DATE_PATTERN.format(this.finish));
        return sb.toString();
    }

}
