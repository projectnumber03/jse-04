package ru.shilov.tm;

import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.enumerated.TerminalCommand;
import ru.shilov.tm.service.IService;
import ru.shilov.tm.service.ProjectServiceImpl;
import ru.shilov.tm.service.TaskServiceImpl;
import ru.shilov.tm.view.IView;
import ru.shilov.tm.view.ProjectViewImpl;
import ru.shilov.tm.view.TaskViewImpl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Bootstrap {

    public static final DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private Scanner scanner = new Scanner(System.in);

    private IService<Project> projectService = new ProjectServiceImpl();

    private IService<Task> taskService = new TaskServiceImpl();

    protected void init() {
        ProjectExecutor pe = new ProjectExecutor();
        TaskExecutor te = new TaskExecutor();
        IView.showWelcomeMsg();
        while (true) {
            try {
                TerminalCommand command = TerminalCommand.valueOf(scanner.nextLine().replace("-", "_").toUpperCase());
                switch (command) {
                    case PROJECT_CLEAR:
                        pe.removeAll();
                        continue;
                    case PROJECT_CREATE:
                        pe.persist();
                        continue;
                    case PROJECT_LIST:
                        pe.find();
                        continue;
                    case PROJECT_EDIT:
                        pe.merge();
                        continue;
                    case PROJECT_REMOVE:
                        pe.remove();
                        continue;
                    case PROJECT_SELECT:
                        pe.select();
                        continue;
                    case TASK_CLEAR:
                        te.removeAll();
                        continue;
                    case TASK_CREATE:
                        te.persist();
                        continue;
                    case TASK_LIST:
                        te.find();
                        continue;
                    case TASK_EDIT:
                        te.merge();
                        continue;
                    case TASK_REMOVE:
                        te.remove();
                        continue;
                    case TASK_SELECT:
                        te.select();
                        continue;
                    case TASK_ATTACH:
                        te.attach();
                        continue;
                    case HELP:
                        IView.showHelpMsg();
                        continue;
                    case EXIT:
                        break;
                }
                break;
            } catch (RuntimeException e) {
                IView.showErrorMsg();
            }
        }
    }

    private class ProjectExecutor {

        private IView view = new ProjectViewImpl();

        private void removeAll() {
            projectService.removeAll();
            view.showClearedMsg();
        }

        private void remove() {
            view.showRemoveMsg();
            view.showInputIdMsg();
            String idProject = projectService.getId(scanner.nextLine());
            projectService.remove(idProject);
            view.showDeletedMsg();
        }

        private void merge() {
            view.showEditMsg();
            Project p = new Project();
            view.showInputIdMsg();
            String idProject = projectService.getId(scanner.nextLine());
            p.setId(idProject);
            view.showInputNameMsg();
            p.setName(scanner.nextLine());
            view.showInputDescriptionMsg();
            p.setDescription(scanner.nextLine());
            view.showInputStartDateMsg();
            p.setStart(LocalDate.parse(scanner.nextLine(), DATE_PATTERN));
            view.showInputEndDateMsg();
            p.setFinish(LocalDate.parse(scanner.nextLine(), DATE_PATTERN));
            projectService.merge(p);
            view.showSuccessMsg();
        }

        private void persist() {
            view.showCreateMsg();
            Project p = new Project();
            view.showInputNameMsg();
            p.setName(scanner.nextLine());
            view.showInputDescriptionMsg();
            p.setDescription(scanner.nextLine());
            view.showInputStartDateMsg();
            p.setStart(LocalDate.parse(scanner.nextLine(), DATE_PATTERN));
            view.showInputEndDateMsg();
            p.setFinish(LocalDate.parse(scanner.nextLine(), DATE_PATTERN));
            projectService.persist(p);
            view.showSuccessMsg();
        }

        private void find() {
            view.showListMsg();
            String idProject = scanner.nextLine();
            if (idProject.equalsIgnoreCase("ALL")) {
                view.print(projectService.getAllProjectsWithTasks());
                view.showSuccessMsg();
                return;
            }
            view.print(projectService.getProjectWithTasks(projectService.getId(idProject)));
            view.showSuccessMsg();
        }

        private void select() {
            view.showSelectMsg();
            view.showInputIdMsg();
            String idProject = projectService.getId(scanner.nextLine());
            view.print(projectService.findOne(idProject).get().toString());
            view.showSuccessMsg();
        }

    }

    private class TaskExecutor {

        private TaskViewImpl view = new TaskViewImpl();

        private void removeAll() {
            view.showClearedMsg();
            taskService.removeAll();
        }

        private void persist() {
            view.showCreateMsg();
            Task t = new Task();
            view.showInputNameMsg();
            t.setName(scanner.nextLine());
            view.showInputDescriptionMsg();
            t.setDescription(scanner.nextLine());
            view.showInputStartDateMsg();
            t.setStart(LocalDate.parse(scanner.nextLine(), DATE_PATTERN));
            view.showInputEndDateMsg();
            t.setFinish(LocalDate.parse(scanner.nextLine(), DATE_PATTERN));
            taskService.persist(t);
            view.showSuccessMsg();
        }

        private void find() {
            view.showListMsg();
            view.print(taskService.getAllTasks());
            view.showSuccessMsg();
        }

        private void merge() {
            view.showEditMsg();
            Task t = new Task();
            view.showInputIdMsg();
            String taskId = taskService.getId(scanner.nextLine());
            t.setId(taskId);
            view.showInputNameMsg();
            t.setName(scanner.nextLine());
            view.showInputDescriptionMsg();
            t.setDescription(scanner.nextLine());
            view.showInputStartDateMsg();
            t.setStart(LocalDate.parse(scanner.nextLine(), DATE_PATTERN));
            view.showInputEndDateMsg();
            t.setFinish(LocalDate.parse(scanner.nextLine(), DATE_PATTERN));
            taskService.merge(t);
            view.showSuccessMsg();
        }

        private void remove() {
            view.showRemoveMsg();
            view.showInputIdMsg();
            String taskId = taskService.getId(scanner.nextLine());
            taskService.remove(taskId);
            view.showDeletedMsg();
        }

        private void select() {
            view.showSelectMsg();
            view.showInputIdMsg();
            String taskId = taskService.getId(scanner.nextLine());
            view.print(taskService.findOne(taskId).get().toString());
        }

        private void attach() {
            view.showAttachMsg();
            view.showInputIdMsg();
            String taskId = taskService.getId(scanner.nextLine());
            Task t = taskService.findOne(taskId).get();
            view.showInputProjectIdMsg();
            t.setIdProject(projectService.getId(scanner.nextLine()));
            view.showSuccessMsg();
        }

    }

}
