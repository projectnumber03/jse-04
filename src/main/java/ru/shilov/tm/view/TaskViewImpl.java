package ru.shilov.tm.view;

import ru.shilov.tm.enumerated.TerminalCommand;

public class TaskViewImpl implements IView {

    @Override
    public void showClearedMsg() {
        System.out.println(String.format("[%s]", TerminalCommand.TASK_CLEAR.getDescription().toUpperCase()));
        System.out.println("[ВСЕ ЗАДАЧИ УДАЛЕНЫ]");
    }

    @Override
    public void showDeletedMsg() {
        System.out.println("[ЗАДАЧА УДАЛЕНА]");
    }

    @Override
    public void showCreateMsg() {
        System.out.println(String.format("[%s]", TerminalCommand.TASK_CREATE.getDescription().toUpperCase()));
    }

    @Override
    public void showListMsg() {
        System.out.println(String.format("[%s]", TerminalCommand.TASK_LIST.getDescription().toUpperCase()));
    }

    @Override
    public void showEditMsg() {
        System.out.println(String.format("[%s]", TerminalCommand.TASK_EDIT.getDescription().toUpperCase()));
    }

    @Override
    public void showRemoveMsg() {
        System.out.println(String.format("[%s]", TerminalCommand.TASK_REMOVE.getDescription().toUpperCase()));
    }

    @Override
    public void showSelectMsg() {
        System.out.println(String.format("[%s]", TerminalCommand.TASK_SELECT.getDescription().toUpperCase()));
    }

    @Override
    public void showInputNameMsg() {
        System.out.println("ВВЕДИТЕ НАЗВАНИЕ ЗАДАЧИ:");
    }

    @Override
    public void showInputIdMsg() {
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
    }

    public void showAttachMsg() {
        System.out.println(String.format("[%s]", TerminalCommand.TASK_ATTACH.getDescription().toUpperCase()));
    }

    public void showInputProjectIdMsg() {
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
    }

}
