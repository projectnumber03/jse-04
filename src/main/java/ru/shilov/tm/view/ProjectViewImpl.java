package ru.shilov.tm.view;

import ru.shilov.tm.enumerated.TerminalCommand;

public class ProjectViewImpl implements IView {

    @Override
    public void showClearedMsg() {
        System.out.println("[ВСЕ ПРОЕКТЫ УДАЛЕНЫ]");
    }

    @Override
    public void showDeletedMsg() {
        System.out.println("[ПРОЕКТ УДАЛЕН]");
    }

    @Override
    public void showCreateMsg() {
        System.out.println(String.format("[%s]", TerminalCommand.PROJECT_CREATE.getDescription().toUpperCase()));
    }

    @Override
    public void showListMsg() {
        System.out.println(String.format("[%s]", TerminalCommand.PROJECT_LIST.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА ИЛИ ВВЕДИТЕ \"ALL\" ДЛЯ ВЫВОДА ПОЛНОГО СПИСКА ПРОЕКТОВ:");
    }

    @Override
    public void showEditMsg() {
        System.out.println(String.format("[%s]", TerminalCommand.PROJECT_EDIT.getDescription().toUpperCase()));
    }

    @Override
    public void showRemoveMsg() {
        System.out.println(String.format("[%s]", TerminalCommand.PROJECT_REMOVE.getDescription().toUpperCase()));
    }

    @Override
    public void showSelectMsg() {
        System.out.println(String.format("[%s]", TerminalCommand.PROJECT_SELECT.getDescription().toUpperCase()));
    }

    @Override
    public void showInputIdMsg() {
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
    }

    @Override
    public void showInputNameMsg() {
        System.out.println("ВВЕДИТЕ НАЗВАНИЕ ПРОЕКТА:");
    }

}
