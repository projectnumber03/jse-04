package ru.shilov.tm.view;

import ru.shilov.tm.enumerated.TerminalCommand;

import java.util.Arrays;

public interface IView {

    default void showSuccessMsg() {
        System.out.println("[OK]");
    }

    default void print(String s) {
        System.out.println(s);;
    }

    static void showWelcomeMsg() {
        System.out.println("*** ДОБРО ПОЖАЛОВАТЬ В ПЛАНИРОВЩИК ЗАДАЧ ***");
    }

    static void showErrorMsg() {
        System.out.println("[ОШИБКА]");
    }

    static void showHelpMsg() {
        Arrays.stream(TerminalCommand.values()).forEach(c -> System.out.println(c.toString().replace("_", "-").toLowerCase() + ": " + c.getDescription()));
    }

    void showClearedMsg();

    void showDeletedMsg();

    void showCreateMsg();

    void showListMsg();

    void showEditMsg();

    void showRemoveMsg();

    void showSelectMsg();

    void showInputIdMsg();

    void showInputNameMsg();

    default void showInputDescriptionMsg() {
        System.out.println("ВВЕДИТЕ ОПИСАНИЕ:");
    }

    default void showInputStartDateMsg() {
        System.out.println("ВВЕДИТЕ ДАТУ НАЧАЛА:");
    }

    default void showInputEndDateMsg() {
        System.out.println("ВВЕДИТЕ ДАТУ ОКОНЧАНИЯ:");
    }

}
